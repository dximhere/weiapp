import {
    Base
} from '../../utils/base.js';

class Home extends Base {
    constructor() {
        super();
    }
    query(com, nu, callback) {
        var param = {
            'url': 'https://m.kuaidi100.com/query?type=' + com + '&postid=' + nu,
            'pickUrl': true,
            sCallback: function (data) {
                callback && callback(data);
            },
        }
        this.request(param);
    }

    /*banner图片信息*/
    getBannerData(bannerid = 1, callback) {
        var that = this;
        var param = {
            url: 'banner/' + bannerid,
            sCallback: function(data) {
                data = data.items;
                callback && callback(data);
            }
        };
        this.request(param);
    }

    getJindu(callback){
        var param = {
            url: 'admission/query',
            type: 'post',
            sCallback: function (data) {
                callback && callback(data);
            }
        }
        this.request(param)
    }

    admission(ks, callback) {
        var param = {
            url:'admission/luqu?XDEBUG_SESSION_START=17068',
            type:'post',
            data:{
                ksh:ks
            },
            sCallback: function (data) {
                callback && callback(data);
            }
        }
        this.request(param)
    }

    testid(id) {
        // 1 "验证通过!", 0 //校验不通过
        var format = /^(([1][1-5])|([2][1-3])|([3][1-7])|([4][1-6])|([5][0-4])|([6][1-5])|([7][1])|([8][1-2]))\d{4}(([1][9]\d{2})|([2]\d{3}))(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))\d{3}[0-9xX]$/;
        //号码规则校验
        if (!format.test(id)) {
            return {
                status: false,
                msg: '身份证号码不合规'
            };
        }
        //区位码校验
        //出生年月日校验   前正则限制起始年份为1900;
        var year = id.substr(6, 4), //身份证年
            month = id.substr(10, 2), //身份证月
            date = id.substr(12, 2), //身份证日
            time = Date.parse(month + '-' + date + '-' + year), //身份证日期时间戳date
            now_time = Date.parse(new Date()), //当前时间戳
            dates = (new Date(year, month, 0)).getDate(); //身份证当月天数
        if (time > now_time || date > dates) {
            return {
                status: false,
                msg: '出生日期不合规'
            }
        }
        //校验码判断
        var c = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //系数
        var b = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'); //校验码对照表
        var id_array = id.split("");
        var sum = 0;
        for (var k = 0; k < 17; k++) {
            sum += parseInt(id_array[k]) * parseInt(c[k]);
        }
        if (id_array[17].toUpperCase() != b[sum % 11].toUpperCase()) {
            return {
                status: false,
                msg: '身份证校验码不合规'
            };
        }
        return {
            status: true,
            msg: '校验通过'
        };
    }

}

export {
    Home
}