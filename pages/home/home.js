// pages/home/home.js

import {
    Home
} from 'home-model.js';

var home = new Home();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        index: 0,
        ks: '',
        idCard: '',
        hidding: true,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        // 获得bannar信息
        home.getBannerData(1, (data) => {
            this.setData({
                bannerArr: data,
            });
        });
        home.getJindu((data) => {
            this.setData({
                jindu: data,
            });
        })
    },

    onShow: function() {
        var lishi = wx.getStorageSync('lishi')
        this.setData({
            lishi: lishi
        })
    },
    ksInput: function(e) {
        var ks = e.detail.value;
        this.setData({
            ks: ks
        })
    },

    idCardInput: function(e) {
        var idCard = e.detail.value;
        this.setData({
            idCard: idCard
        })
    },
    getuserinfo(event) {
        var ks = this.data.ks
        // var idCard = this.data.idCard
        if (ks == null) return
        // if (idCard == null) return
        var ksReg = /^[0-9]{14}$/;
        var idCardReg = /(\d{15}$)|(\d{18}$)|(^\d{17}(X|x)$)/;
        if (!ksReg.test(ks)) {
            wx.showModal({
                title: '提示',
                content: '请出入14位考生号',
            })
            return;
        }
        wx.showLoading({
            title: '加载中',
        })
        setTimeout(function() {
            wx.hideLoading()
        }, 500)
        console.log(event)
        var userInfo = event.detail.userInfo;
        if (userInfo) {
            wx.setStorageSync('userInfo', userInfo)
            home.admission(ks, (res) => {
                if (res.msg) {
                    wx.setStorageSync('ks', res.data)
                    wx.setStorageSync('lishi', res.data.ksh)
                    var lishi = wx.getStorageSync('lishi')
                    var newlishi = lishi.push(res.data.ksh);
                    wx.setStorageSync('lishi', newlishi)
                    wx.navigateTo({
                        url: '../luqu/luqu',
                    })
                } else {
                    wx.showModal({
                        title: '提示',
                        content: res.data,
                    })
                }

            })
        } else {
            wx.hideLoading()
            wx.showModal({
                title: '提示',
                content: '请允许微信授权',
            })
            return;
        }

    },
    qulu() {
        var ks = this.data.ks
        // var idCard = this.data.idCard
        if (ks == null) return
        // if (idCard == null) return
        var ksReg = /^[0-9]{14}$/;
        var idCardReg = /(\d{15}$)|(\d{18}$)|(^\d{17}(X|x)$)/;
        if (!ksReg.test(ks)) {
            wx.showModal({
                title: '提示',
                content: '请出入14位考生号',
            })
            return;
        }
        wx.showLoading({
            title: '加载中',
        })
        setTimeout(function() {
            wx.hideLoading()
        }, 100000)
        home.admission(ks, (res) => {
            wx.hideLoading()
            if (res.msg) {
                wx.setStorageSync('ks', res.data)
                var lishi = wx.getStorageSync('lishi')
                var newlishi = [];
                for (var i = 0; i < lishi.length; i++) {
                    if (lishi.length > 9)
                        continue;
                    if (lishi[i] != null && lishi[i] != res.data.ksh) {
                        newlishi.push(lishi[i]);
                    }
                }
                newlishi.push(res.data.ksh);
                wx.setStorageSync('lishi', newlishi)
                wx.navigateTo({
                    url: '../luqu/luqu',
                })
            } else {
                wx.showModal({
                    title: '提示',
                    content: res.data,
                })
            }

        })
    },
    checkoutStudent() {


    },
    lishi(e) {
        console.log(e)
        var ks = e.target.id;
        this.setData({
            ks: ks
        })
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {
        return {
            title: '四川理工学院录取查询',
            path: '/pages/home/home'
        }
    }
})