// pages/luqu/luqu.js

import {
    Home
} from '../home/home-model.js';
const { $Message } = require('../../dist/base/index');
var home = new Home();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        data: null,
        kdShow:false,
        kdSuccess:true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function(options) {
        var ks = wx.getStorageSync('ks')
        this.setData({
            data: ks,
        })
        if(ks.kd){
            this.kd()
        }
    },

    kdReload(){
        this.kd()
    },
    kd(){
        this.setData({
            kdShow: false,
            kdSuccess: true
        })
        $Message({
            content: '正在为您查询物流信息'
        });
        setTimeout(this.doquery,1000)
        setTimeout(()=>{
            if(!this.data.kdMsg){
                $Message({
                    content: '网络超时···'
                });
                this.setData({
                    kdShow: true,
                    kdSuccess: false
                })
            }
        }, 10000)
    },

    doquery(){
        var ks = wx.getStorageSync('ks')
        home.query('ems', ks.kd, (res) => {
            this.setData({
                kdShow: true
            })
            if (res.message === 'ok') {
                $Message({
                    content: '查询成功'
                });
                this.setData({
                    kdMsg: res.data
                })
            } else {
                this.setData({
                    kdSuccess: false
                })
                if (res.message) {
                    $Message({
                        content: res.message
                    });
                } else {
                    $Message({
                        content: '查询失败，请访问ems官网查询'
                    });
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        // this.drow()
    
    },

    drow(){
        var that = this
        wx.downloadFile({
            url: this.data.userInfo.avatarUrl, //仅为示例，并非真实的资源
            success: function (res) {
                // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
                if (res.statusCode === 200) {
                    that.setData({
                        touxiang: res.tempFilePath
                    })
                    const ctx = wx.createCanvasContext('mycanvas')
                    console.log(that.data)
                    var touxiangX = that.data.windowWidth / 2
                    var touxiangy = 25
                    ctx.drawImage(that.data.touxiang, touxiangX - 25, 10, 50, 50)
                    // ctx.setFontSize(20)
                    // ctx.fillText(that.data.userInfo.nickName, 20, 20)
                    // ctx.fillText('MINA', that.data.windowWidth - 200, 100)
                    ctx.draw()
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function() {
        return {
            title: '四川理工学院录取查询',
            path: '/pages/home/home'
        }
    }
})