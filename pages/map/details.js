// pages/map/details.js
import { Home } from '../home/home-model.js';

var home = new Home();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		building_id:0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		var building_id = parseInt(options.building_id)
		var building
		if (!building_id){
			wx.navigateTo({
				url: 'map',
			})
		}else{
			var build = wx.getStorageSync('building')
			for (var b in build) {
				for (var i in build[b].building_items) {
					if (build[b].building_items[i].building_id == building_id) {
						var building = build[b].building_items[i];
						break
					}
				}
			}
			this.setData({
				building: building,
				building_id: building_id,
			})
			var banner_id = building.building_img_banner_id
			if (banner_id) {
				home.getBannerData(banner_id, (data) => {
					this.setData({
						bannerArr: data,
					});
				});
			}
		}
		

	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})