import { Base } from '../../utils/base.js';

class Map extends Base{
	constructor(){
		super();
	}

	init(callback){
		var param = {
			url:'map/init',
			sCallback:function(data){
				callback&&callback(data)
			}
		}
		this.request(param)
	}

}

export {Map}