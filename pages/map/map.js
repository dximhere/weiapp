// pages/map/map.js
import { Config } from '../../utils/config.js';
import { Map } from './map-model.js';
var map = new Map()
var QQMapWX = require('./qqmap-wx-jssdk.js');

var qqmapsdk;
Page({
	data: {
		markers: [],
		polyline: [{
			points: [{
				longitude: 29.3322400000,
				latitude: 104.7656100000
			}],
			color: "#FF0000DD",
			width: 3,
			dottedLine: true
		}],
		isSelectedBuildlingClass: 0,
		isSelectedBuild: 0,
		isSelectedBuildJust: null,
		clickChangeSchool: false,
		school_index: 0,
		fullscreen: false,
	},
	onShow: function () {
		var buildingId = wx.getStorageSync('backtomap')
		var that = this
		if (buildingId>0) {
			console.log(buildingId)
			let building = wx.getStorageSync('building')
			console.log(building)
			for (var b in building) {
				for (var i in building[b].building_items) {
					if (building[b].building_items[i].building_id == buildingId) {
						console.log(building[b].building_items[i])
						that.setData({
							isSelectedBuildlingClasss: b,
							isSelectedBuildlingClass:b,
							school_index: building[b].building_items[i].school_id - 1,
						})
					}
				}
			}
			wx.setStorageSync('backtomap',0)
			that._loadmarkers();
		}
		
	},
	onLoad: function (options) {
		var that = this
		this.mapCtx = wx.createMapContext('map')
		map.init((data) => {
			that.setData({
				latitude: data.mapSet.map_center_latitude,
				longitude: data.mapSet.map_center_longitude,
				scale: data.mapSet.scale,
				building: data.building,
				schools: data.schools,
			})
			wx.setStorageSync('building', data.building)
			wx.getSystemInfo({
				success: function (res) {
					that.setData({
						windowHeight: res.windowHeight,
						windowWidth: res.windowWidth
					})
					that.setControls(res.windowWidth, res.windowHeight / 2)
				},
			})
			that._loadmarkers();
		})

	},
	_loadmarkers() {
		var building = this.data.building;
		var markers = [];
		var buildingItem = [];
		var buildlingClass = this.data.isSelectedBuildlingClass
		var school = this.data.school_index + 1;
		this.setData({
			markers: markers,
			buildingItem: buildingItem,
		})
		if (building[buildlingClass].building_items.length > 0) {
			for (let i = 0; i < building[buildlingClass].building_items.length; i++) {
				if (building[buildlingClass].building_items[i].school_id == school) {
					if (building[buildlingClass].building_items[i].show_name == 1) {
						var marker = {
							iconPath: '../../img/icon/' + building[buildlingClass].icon,
							id: building[buildlingClass].building_items[i].building_id,
							latitude: building[buildlingClass].building_items[i].latitude,
							longitude: building[buildlingClass].building_items[i].longitude,
							width: 32,
							height: 32,
							label: {
								content: building[buildlingClass].building_items[i].building_simple_name,
								color: '#118eea',
							}
						}
					} else {
						var marker = {
							iconPath: '../../img/icon/' + building[buildlingClass].icon,
							id: building[buildlingClass].building_items[i].building_id,
							latitude: building[buildlingClass].building_items[i].latitude,
							longitude: building[buildlingClass].building_items[i].longitude,
							width: 32,
							height: 32,
						}
					}
					var build = building[buildlingClass].building_items[i];
					buildingItem.push(build)
					markers.push(marker)
				}
			}
			var points = []
			for (let i = 0; i < markers.length; i++) {
				var p = {
					latitude: markers[i].latitude,
					longitude: markers[i].longitude,
				}
				points.push(p)
			}
			var includePoints = {
				points: points,
				padding: [80]
			}
			this.mapCtx.includePoints(includePoints)
			this.setData({
				markers: markers,
				buildingItem: buildingItem
			})
		}


	},
	clickButton: function (e) {
		this.setData({ fullscreen: !this.data.fullscreen, clickChangeSchool: false })
		if (this.data.fullscreen) {
			this.setControls(this.data.windowWidth, this.data.windowHeight - 80)
		} else {
			this.setControls(this.data.windowWidth, this.data.windowHeight / 2)
		}
	},
	setControls: function (width, height) {
		this.setData({
			controls: [{
				id: -1,
				iconPath: '../../img/icon/sousuo.png',
				position: {
					left: width - 50,
					top: height - 110,
					width: 40,
					height: 40
				},
				clickable: true
			}, {
				id: -2,
				iconPath: '../../img/icon/suoding.png',
				position: {
					left: width - 50,
					top: height - 65,
					width: 40,
					height: 40
				},
				clickable: true
			}]
		})
	},
	regionchange(e) {
		console.log(e.type)
		if (e.type == 'end') {
			this.setData({
				clickChangeSchool: false,
			})
		}

	},
	markertap(e) {
		console.log(e.markerId)
		var buildingItem = this.data.buildingItem
		for (let i = 0; i < buildingItem.length; i++) {
			if (e.markerId == buildingItem[i].building_id) {
				this.setData({
					isSelectedBuild: i,
					isSelectedBuildJust: i
				})
			}
		}
	},
	controltap(e) {
		console.log(e.controlId)
		var id = e.controlId
		if (id == -1) {
			wx.navigateTo({
				url: 'serach',
			})
		}
		if (id == -2) {
			this.mapCtx.moveToLocation()
		}

	},
	changeBuildingClass(e) {
		this.setData({
			isSelectedBuildlingClass: e.currentTarget.id,
			isSelectedBuild: 0
		})
		this._loadmarkers();
	},
	school() {
		this.setData({
			clickChangeSchool: !this.data.clickChangeSchool,
		})

	},
	changeSchool(e) {
		this.setData({
			school_index: e.currentTarget.id,
		})
		this._loadmarkers();
	},
	changeBuilding(event) {
		this.setData({
			isSelectedBuildJust: event.currentTarget.id,
		})
	},
	tolocation(event) {
		var id = event.currentTarget.id
		var building = this.data.buildingItem
		var points = []
		var p = {
			latitude: building[id].latitude,
			longitude: building[id].longitude,
		}
		points.push(p)
		var includePoints = {
			points: points,
			padding: [80]
		}
		this.mapCtx.includePoints(includePoints)
	}
})